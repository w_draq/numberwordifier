package com.mavomi.constants;

public class Constants {
	public static final String[] words_0x = {
		"","one","two","three","four","five","six","seven","eight","nine"
	};
	public static final String[] words_1x = {
		"ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"
	};
	public static final String[] words_x0 = {
		"twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"
	};

	public static final String[] words_0xth = {
		"","first","second","third","fourth","fifth","sixth","seventh","eighth","ninth"
	};
	public static final String[] words_1xth = {
		"tenth","eleventh","twelfth","thirteenth","fourteenth","fifteenth","sixteenth","seventeenth","eighteenth","nineteenth"
	};
	public static final String[] words_x0th = {
		"twentieth","thirtieth","fortieth","fiftieth","sixtieth","seventieth","eightieth","ninetieth"
	};

	public static final String AND = "and ";
	public static final String MILLION = "million ";
	public static final String THOUSAND = "thousand ";
	public static final String HUNDRED = "hundred ";
	public static final String TH = "th";
	
}
