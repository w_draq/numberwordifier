package com.mavomi.stringutils;

import com.mavomi.constants.WordifiedNumberLocale;

public interface WordifiedNumberCreator {
	WordifiedNumber create(WordifiedNumberLocale wordifiedNumberLocale);
}
