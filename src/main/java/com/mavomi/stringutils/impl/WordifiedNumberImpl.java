package com.mavomi.stringutils.impl;

import static com.mavomi.stringutils.validator.WordifiedNumberInputValidator.*;

import com.mavomi.constants.Constants;
import com.mavomi.stringutils.WordifiedNumber;

public class WordifiedNumberImpl implements WordifiedNumber {

	private StringBuilder wordifiedNumber;

	/**
	 * {@inheritDoc}
	 */
	public String toWords(int input) {
		
		validate(input);
		
		this.wordifiedNumber = new StringBuilder();
		String result = wordify(input);
		return result;
	}

	private String wordify(int inputNumber) {

		int inputQuotient = 0;
		int inputModulo = 0;

		if (inputNumber >= 1000000) {
			inputQuotient = inputNumber / 1000000;
			inputModulo = inputNumber % 1000000;

			processHundred(inputQuotient);

			wordifiedNumber.append(Constants.MILLION);
			if (inputModulo < 100 && inputModulo > 0) {
				wordifiedNumber.append(Constants.AND);
			}

			wordify(inputModulo);
		} else if (inputNumber >= 1000) {
			inputQuotient = inputNumber / 1000;
			inputModulo = inputNumber % 1000;

			processHundred(inputQuotient);

			wordifiedNumber.append(Constants.THOUSAND);
			if (inputModulo < 100 && inputModulo > 0) {
				wordifiedNumber.append(Constants.AND);
			}

			wordify(inputModulo);
		} else if (inputNumber >= 1) {
			inputQuotient = inputNumber % 1000;
			processHundred(inputQuotient);
		}
		return wordifiedNumber.toString().trim();
	}

	private void processHundred(int hNumber) {
		
		int decNumberQuotient = 0;
		int decNumberModulo = 0;

		int hNumberQuotient = (int) (hNumber / 100);
		int hNumberModulo = hNumber % 100;
		
		if (hNumber >= 100) {

			wordifiedNumber.append(Constants.words_0x[hNumberQuotient] + " "
					+ Constants.HUNDRED);
			if (hNumberModulo > 0) {
				wordifiedNumber.append(Constants.AND);
			}
		}
		if (hNumberModulo > 0 && hNumberModulo < 10) {
			wordifiedNumber.append(Constants.words_0x[hNumberModulo] + " ");
		} else if (hNumberModulo >= 10 && hNumberModulo < 20) {
			wordifiedNumber.append(Constants.words_1x[hNumberModulo - 10] + " ");
		} else if (hNumberModulo >= 20) {
			decNumberQuotient = hNumberModulo / 10;
			decNumberModulo = hNumberModulo % 10;
			wordifiedNumber.append(Constants.words_x0[decNumberQuotient - 2] + " ");
			wordifiedNumber.append(Constants.words_0x[decNumberModulo] + " ");
		}
	}

}
