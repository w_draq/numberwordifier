package com.mavomi.stringutils.impl;

import com.mavomi.constants.WordifiedNumberLocale;
import com.mavomi.stringutils.WordifiedNumber;
import com.mavomi.stringutils.WordifiedNumberCreator;

public class WordifiedNumberCreatorImpl implements WordifiedNumberCreator {

	public WordifiedNumber create(WordifiedNumberLocale wordifiedNumberLocale) {
		WordifiedNumber wordifiedNumber;
		switch (wordifiedNumberLocale) {
		case EN:
			wordifiedNumber = new WordifiedNumberImpl();
			break;
		default:
			wordifiedNumber = new WordifiedNumberImpl();
			break;
		}
		return wordifiedNumber;
	}

}
