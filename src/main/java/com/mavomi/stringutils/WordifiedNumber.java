package com.mavomi.stringutils;

/**
 * @author Wojciech Draczyński <wojciech.draczynski@gmail.com>
 *
 */
public interface WordifiedNumber {

	/**
	 * Converts a number to equivalent number in words
	 * @param number
	 * @return the equivalent number in words
	 */
	String toWords(int number);
}
