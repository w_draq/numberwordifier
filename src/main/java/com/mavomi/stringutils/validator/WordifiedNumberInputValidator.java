package com.mavomi.stringutils.validator;

public class WordifiedNumberInputValidator {
	/**
	 * Validates input number
	 * valid range: 1 - 999 999 999
	 * @param input number
	 */
	public static void validate(int input) {
		if (input > 999999999 || input <= 0) {
			throw new IllegalArgumentException();
		}
	}
}
