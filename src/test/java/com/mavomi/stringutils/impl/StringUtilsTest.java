package com.mavomi.stringutils.impl;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mavomi.constants.WordifiedNumberLocale;
import com.mavomi.stringutils.WordifiedNumber;
import com.mavomi.stringutils.WordifiedNumberCreator;

public class StringUtilsTest {
	
	private static WordifiedNumber wordifiedNumber;
	
	@BeforeClass
	public static void setUp() {
		WordifiedNumberCreator creator = new WordifiedNumberCreatorImpl();
		wordifiedNumber = creator.create(WordifiedNumberLocale.EN);
	}
	
	@Test
	public void wordifiedWordReferenceTest() throws Exception {
		
		assertEquals("one hundred and five", wordifiedNumber.toWords(105));
		assertEquals("one thousand and five", wordifiedNumber.toWords(1005));
		assertEquals("one hundred and ten thousand one hundred and five", wordifiedNumber.toWords(110105));
		assertEquals("one million and ninety nine", wordifiedNumber.toWords(1000099));
		assertEquals("one million one hundred", wordifiedNumber.toWords(1000100));
		assertEquals("fifty six million nine hundred and forty five thousand seven hundred and eighty one",
				wordifiedNumber.toWords(56945781));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void wordifiedWordInvalidNegativeInputTest() {
		wordifiedNumber.toWords(-2432);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void wordifiedWordInvalidZeroInputTest() {
		wordifiedNumber.toWords(0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void wordifiedWordInvalidOverrangedInputTest() {
		wordifiedNumber.toWords(1000000000);
	}
}
